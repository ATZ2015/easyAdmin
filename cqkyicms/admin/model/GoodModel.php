<?php
/**
 * 重庆柯一网络有限公司 版权所有
 * 开发团队:柯一网络 柯一CMS项目组
 * 创建时间: 2018/5/13 10:07
 * 联系电话:023-52889123 QQ：563088080
 * 惟一官网：www.cqkyi.com
 */

namespace app\admin\model;


use app\admin\validate\GoodValidate;
use think\Model;

class GoodModel extends Model
{

    protected $name="good";


    public function getByWhere($where, $offset, $limit)
    {
        return $this->alias('u')->field( 'u.*,cate_name')
            ->join('good_cate rol', 'u.cate_id = ' . 'rol.cate_id')
            ->where($where)->limit($offset, $limit)->order('good_id desc')->select();
    }

    public function getAll($where)
    {
        return $this->where($where)->count();
    }


    /*
     * 添加商品
     */

    public function add($data){
        try {
            $validate  = new GoodValidate();
            if (!$validate->check($data)) {
                return easymsg(2,'',$validate->getError());
            }
            $data['creattime']=time();

            $this->save($data);
            return easymsg(1,url('good/index'),'添加商品成功');
        }catch(PDOException $e){
            return easymsg(-1,'',$e->getMessage());
        }
    }
}